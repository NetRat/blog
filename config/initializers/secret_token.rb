# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Workspace::Application.config.secret_key_base = 'ebd8b9bb47e0983560c44e930e870efd4c00d358895331ab4362e0bb7c0e4d13089255d786ba96fdd38d65c5acef9839b3763311ab49f848147d32a3913ab0a2'
